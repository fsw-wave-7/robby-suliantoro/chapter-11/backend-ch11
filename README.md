Create a new repository

git clone https://gitlab.com/fsw-wave-7/back-end-chapter-10.git
cd back-end-chapter-10
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main

------

Push an existing folder

cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/fsw-wave-7/back-end-chapter-10.git
git add .
git commit -m "Initial commit"
git push -u origin main

-----

Push an existing Git repository

cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/fsw-wave-7/back-end-chapter-10.git
git push -u origin --all
git push -u origin --tags