'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Cities',
      [
        {
          name: 'Jakarta',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'Bogor',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'Bandung',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'Bekasi',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'Purwokerto',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'Yogyakarta',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'Surabaya',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'Balikpapan',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'Pontianak',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
