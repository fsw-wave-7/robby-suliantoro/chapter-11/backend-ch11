'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Games',
      [
        {
          name: 'Rock, Paper, Scissors',
          description: 'game jadul yang perlu anda coba',
          img_url: '/images/rock-paper-scissors.png',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'poker',
          description: 'game yang cocok untuk menghabiskan waktu',
          img_url: '/images/poker.png',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
